package PeertoPeer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/**
 * Klasa tworząca wątek klienta, nawiązujący połączenie z socketem serwera
 *
 * @author Arkadiusz Sięda
 * @version 1.0
 */
public class PeerThread extends Thread {

    /**
     * Czyta dane z strumienia wyjściowego socketu serwera
     */
    private final BufferedReader bufferedReader;

    /**
     * Obiekt klasy PeerPanel, okno chatu
     * @see PeerPanel
     */
    private final PeerPanel peerPanel;

    /**
     * Konstruktor klasy PeerThread, tworzący połączenie z serwerem i odbierający z niego wiadomości
     *
     * @param socket Socket serwera, z którym nawiązno połączenie
     * @param peerPanel Obiekt klasy PeerPanel, okno chatu
     * @throws IOException W przypadku problemów z połączeniem z socketem serwera
     * @see PeerPanel
     */
    public PeerThread(Socket socket, PeerPanel peerPanel) throws IOException {
        bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.peerPanel = peerPanel;
    }
    /**
     * Metoda określająca działanie wątku PeerThread - odbiera dane z socketa serwera i wyświetla w oknie chatu
     */
    public void run() {
        boolean flag = true;
        while (flag) {
            try {
                String inputLine;
                while ((inputLine = bufferedReader.readLine()) != null) {
                    peerPanel.addToChat(inputLine);
                }

            } catch (Exception e) {
                peerPanel.addToChat("Użytkownik się rozłączył. Możesz naciśnąć przycisk 'Nowa romowa' aby zacząć czat z innym użykownikiem.");
                flag = false;
                interrupt();

            }
        }
    }
}
