package PeertoPeer;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import java.time.LocalTime;
import static java.awt.Toolkit.getDefaultToolkit;

/**
 * Klasa wyświetlająca okno chatu
 *
 * @author Arkadiusz Sięda
 * @version 1.0
 */
public class PeerPanel extends JFrame {

    /**
     * Pole wyświetlające konwersację z innym użytkownikiem
     */

    private final JTextArea chatWindow;

    /**
     * Scroll dla pola chatWindow
     */
    private final JScrollPane scrollChatWindow;

    /**
     * Pole do wpisywania wiadomości do wysłania
     */
    private final JTextField messageField;

    /**
     * Przycisk wysyłający wiadomość z pola messageField do innego użytkownika
     */
    private final JButton sendButton;

    /**
     * Przycisk do zamknięcia aktualnej rozmowy i wywołania okna obiektu klasy "ConnectWindow" w celu połączenia z nowym użytkownikiem
     * @see ConnectWindow
     */
    private final JButton reconnectButton;

    /**
     * Panel do umieszczenia scrolowalnego pola chatWindow
     */
    private final JPanel chatPanel;


    /**
     * Konstruktor klasy PeerPanel tworzący i wyświetlający okno chatu
     * @param serverThread Obiekt klasy ServerThread
     * @param username Nazwa użytkownika
     * @see ServerThread
     */
    public PeerPanel(ServerThread serverThread, String username) {
        super(username + " chat");

        chatWindow = new JTextArea();
        chatWindow.setEditable(false);
        scrollChatWindow = new JScrollPane(chatWindow);
        messageField = new JTextField();
        sendButton = new JButton("Wyślij");
        reconnectButton = new JButton("Nowa rozmowa");

        chatPanel = new JPanel(new BorderLayout());
        setLayout(new GridBagLayout());
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(600, 1000);
        scrollChatWindow.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        Dimension dim = getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

        chatPanel.add(scrollChatWindow);

        Border loweredLevel;
        loweredLevel = BorderFactory.createLoweredBevelBorder();
        chatPanel.setBorder(loweredLevel);

        reconnectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int portNumber = serverThread.getSocketLocalPort();
                serverThread.sendMessage(username + " opuścił rozmowę, naciśnij przycik 'Nowa rozmowa' aby rozpocząć czat z innym użykownikiem");
                serverThread.socketClose();
                ServerThread serverThread = null;
                try {
                    serverThread = new ServerThread(portNumber);
                } catch (IOException e2) {
                    e2.printStackTrace();
                }
                dispose();
                if (serverThread != null) {
                    serverThread.start();
                    ConnectWindow connectWindow = new ConnectWindow(serverThread, username);
                }
            }
        });

        sendButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e){
                String messageToSend = messageField.getText();
                LocalTime localTime = LocalTime.now();
                serverThread.sendMessage("[" + localTime.withNano(0) + "] " + username + ": " + messageToSend);
                addToChat("[" + localTime.withNano(0) + "] You: " + messageToSend);
                messageField.setText("");
            }
        });
        messageField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {
            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                if (keyEvent.getKeyChar() == '\n') {
                    sendButton.doClick();
                }
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
            }
        });

        add(reconnectButton, new GridBagConstraints(4, 0, 4, 1, 0.1, 0.1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0), 0, 0));
        add(chatPanel, new GridBagConstraints(0, 1, 6, 6, 1, 1,
                GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                new Insets(0, 0, 0, 0), 0, 0));
        add(messageField, new GridBagConstraints(0, 7, 5, 1, 0.9, 0.1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0), 0, 0));
        add(sendButton, new GridBagConstraints(5, 7, 1, 1, 0.1, 0.1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0), 0, 0));

        setVisible(true);
    }

    /**
     * Metoda klasy PeerPanel dodająca tekst do pola chatWindow
     * @param s Tekst do wyświetlenia
     */
    public void addToChat(String s) {
        Font font = new Font("Verdana", Font.BOLD, 10);
        chatWindow.setFont(font);
        chatWindow.append(s + "\n");
    }
}
