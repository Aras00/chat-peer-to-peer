package PeertoPeer;

import java.io.IOException;
import java.net.Socket;

/**
 * Klasa startowa, uruchamiająca program "Chat Peer-to-Peer"
 *
 * @author Arkadiusz Sięda
 * @version 1.0
  */
public class Peer {

    /**
     * Metoda główna klasy Peer, tworzy obiekt klasy LoginWindow
     * @param args tablica arumnetów wejściowych
     * @see LoginWindow
     */
    public static void main(String[] args) {

        LoginWindow loginWindow = new LoginWindow();

    }

    /**
     * Metoda tworząca obiekt klasy Socket oraz obiekt klasy PeerPanel
     * @param serverThread Obiekt klasy ServerThread
     * @param username Nazwa użytkownika
     * @param address Adres IP użytkownika, z którym ma nastapić połączenie
     * @param portNumber Numer portu użytkownika, z którym ma nastąpić połączenie
     * @return Informacja czy obiekty klas ServerThread i PeerPanel zostały utworzone
     * @see Socket
     * @see PeerPanel
     * @see ServerThread
     */
    public boolean peerPanelStart(ServerThread serverThread, String username, String address, int portNumber) {
        Socket socket = null;
        try {
            socket = new Socket(address, portNumber);

            PeerPanel peerPanel = new PeerPanel(serverThread, username);
            peerPanel.addToChat("Witaj " + username + ". Połączyłeś się z użytkownikiem o adresie: " + address + ":" + portNumber + "\n");
            new PeerThread(socket, peerPanel).start();

        } catch (Exception e) {
            if (socket != null) {
                try {
                    socket.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
            return false;
        }
            return true;
    }
}
