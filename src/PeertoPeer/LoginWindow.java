package PeertoPeer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.IOException;
import static java.awt.Toolkit.getDefaultToolkit;


/**
 * Klasa wyświetlająca okno do logowania
 *
 * @author Arkadiusz Sięda
 * @version 1.0
 */
public class LoginWindow extends JFrame {

    /**
     * Nazwa użytkownika
     */
    private final JTextField loginField;

    /**
     * Numer portu dla serwera
     */
    private final JTextField portNumberField;

    /**
     * Przycisk logujący użytkownika
     */
    private final JButton loginButton;

    /**
     * Napis informacyjny 'Nazwa użytkownika:' dla loginField
     */
    private final JLabel loginLabel;

    /**
     * Napis informacyjny 'Numer portu:' dla portNumberField
     */
    private final JLabel portLabel;

    /**
     * Konstruktor klasy LoginWindow tworzący i wyświetlający okno do logowania
     *
     */
    public LoginWindow() {
        super("Okno logowania");

        loginField = new JTextField();
        portNumberField = new JTextField();
        loginButton = new JButton("Login");
        loginLabel = new JLabel("Nazwa użytkownika:");
        portLabel = new JLabel("Numer portu:");

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(300, 150);
        Dimension dim = getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

        loginButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String username;
                int portNumber;
                username = loginField.getText();
                portNumber = Integer.parseInt(portNumberField.getText());
                ServerThread serverThread = null;
                try {
                    serverThread = new ServerThread(portNumber);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                if (serverThread != null) {
                    serverThread.start();
                    ConnectWindow connectWindow = new ConnectWindow(serverThread, username);
                    setVisible(false);
                    dispose();
                }
            }
        });

        loginField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                if (keyEvent.getKeyChar() == '\n') {
                    loginButton.doClick();
                }
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
            }
        });
        portNumberField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                if (keyEvent.getKeyChar() == '\n') {
                    loginButton.doClick();
                }
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
            }
        });

        setLayout(new GridBagLayout());
        add(loginLabel, new GridBagConstraints(0, 0, 1, 1, 0.1, 0.1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0), 0, 0));
        add(loginField, new GridBagConstraints(1, 0, 4, 1, 0.1, 0.1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0), 0, 0));
        add(portLabel, new GridBagConstraints(0, 2, 1, 1, 0.1, 0.1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0), 0, 0));
        add(portNumberField, new GridBagConstraints(1, 2, 4, 1, 0.1, 0.1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0), 0, 0));
        add(loginButton, new GridBagConstraints(0, 3, 5, 1, 0.1, 0.1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0), 0, 0));

        setVisible(true);
    }

}
