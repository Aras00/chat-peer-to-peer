package PeertoPeer;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import static java.awt.Toolkit.getDefaultToolkit;

/**
 * Klasa wyświetlająca okno do nawiazania połączenia z drugim użytkownikiem
 *
 * @author Arkadiusz Sięda
 * @version 1.0
 */
public class ConnectWindow extends JFrame {

    /**
     * Napis informujący użytkowanika o prawidłowym zalogowaniu
     */
    private final JLabel welcome;

    /**
     * Adres IP użytkownika, z którym ma być nawiązane połączenie
     */
    private final JTextField iPAddressField;

    /**
     * Numer portu użytkownika, z którym ma być nawiązane połączenie
     */
    private final JTextField portNumberField;

    /**
     * Przycisk do nawiązania połączenia z użytkownikiem
     */
    private final JButton connectButton;

    /**
     * Napis informacyjny 'IP address' dla pola IPAddressField
     */

    private final JLabel iPAddressLabel;
    /**
     * Napis informacyjny 'port number' dla pola PortNumberField
     */

    private final JLabel portLabel;
    /**
     * Ostrzeżenie w przypadku nieudanej próby połączenia
     */

    private final JFrame warning;

    /**
     * Konstruktor klasy ConnectWindow, tworzący i wyświetlający okno do nawiązania połączenia
     *
     * @param serverThread Obiekt klasy ServerThread
     * @param username Nazwa użytkownika (zalogowanego)
     * @see ServerThread
     */
    public ConnectWindow(ServerThread serverThread, String username) {
        super("Connection Window");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        welcome = new JLabel ("Jesteś zalogowany jako " + username + " podaj adres IP i port użytkownika, z którym chcesz się połączyć");
        iPAddressField = new JTextField();
        portNumberField = new JTextField();
        connectButton = new JButton("Login");
        iPAddressLabel = new JLabel("IP address:");
        portLabel = new JLabel("port number:");
        warning = new JFrame("No connection");

        setSize(600, 200);
        Dimension dim = getDefaultToolkit().getScreenSize();
        this.setLocation(dim.width/2-this.getSize().width/2, dim.height/2-this.getSize().height/2);

        connectButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                String IPAddress;
                int portNumber;
                IPAddress = iPAddressField.getText();
                portNumber = Integer.parseInt(portNumberField.getText());
                boolean flag = new Peer().peerPanelStart(serverThread, username, IPAddress, portNumber);
                if (flag) {
                    setVisible(false);
                    dispose();
                } else {
                    JOptionPane.showMessageDialog(warning, "Nie można połączyć się z danym użytkownikem (Błędny adres IP / port lub użytkownik jest offline. Podaj inne dane lub spróbuj później.");
                }
            }
        });

        iPAddressField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                if (keyEvent.getKeyChar() == '\n') {
                    connectButton.doClick();
                }
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {

            }
        });
        portNumberField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent keyEvent) {

            }

            @Override
            public void keyPressed(KeyEvent keyEvent) {
                if (keyEvent.getKeyChar() == '\n') {
                    connectButton.doClick();
                }
            }

            @Override
            public void keyReleased(KeyEvent keyEvent) {
            }
        });

        setLayout(new GridBagLayout());
        add(welcome, new GridBagConstraints(0, 0, 6, 1, 0.1, 0.1,
                GridBagConstraints.CENTER, GridBagConstraints.VERTICAL,
                new Insets(0, 0, 0, 0), 0, 0));
        add(iPAddressLabel, new GridBagConstraints(0, 1, 1, 1, 0.1, 0.1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0), 0, 0));
        add(iPAddressField, new GridBagConstraints(1, 1, 2, 1, 0.1, 0.1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0), 0, 0));
        add(portLabel, new GridBagConstraints(0, 3, 1, 1, 0.1, 0.1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0), 0, 0));
        add(portNumberField, new GridBagConstraints(1, 3, 2, 1, 0.1, 0.1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0), 0, 0));
        add(connectButton, new GridBagConstraints(0, 4, 5, 1, 0.1, 0.1,
                GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL,
                new Insets(0, 0, 0, 0), 0, 0));

        setVisible(true);
    }
}
