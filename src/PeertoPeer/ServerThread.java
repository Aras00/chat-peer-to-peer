package PeertoPeer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Klasa tworząca wątek serwera, oczekujący na połaczenie od klienta
 *
 * @author Arkadiusz Sięda
 * @version 1.0
 */
public class ServerThread extends Thread {

    /**
     * Socket serwera
     */
    private final ServerSocket serverSocket;

    /**
     * Wyświetla dane w strumieniu wyjściowym socketu serwera
     */
    private PrintWriter printWriter;

    /**
     * Czyta dane z strumienia wejściowego socketu serwera
     */
    private BufferedReader bufferedReader;

    /**
     * Konstruktor klasy ServerThread, tworzący socket serwera na podanym porcie
     *
     * @param portNumber Numer portu, do stworzenia socketu serwera
     * @throws IOException W przypadku problemów z nawiązaniem połączenia klienta z socketem serwera
     */
    public ServerThread(int portNumber) throws IOException {
        serverSocket = new ServerSocket(portNumber);
    }

    /**
     * Metoda określająca działanie wątku ServerThread - prowadzi nasłuch na sockecie i wysyła wiadomości do klienta
     */
    @Override
    public void run() {
        try {
            Socket socket = serverSocket.accept();
            bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            printWriter = new PrintWriter(socket.getOutputStream(), true);
            while (bufferedReader.readLine() != null) {
                sendMessage(bufferedReader.readLine());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda klasy ServerThread wyświetlająca wiadomość na strumieniu wyjściowym socketu serwera
     *
     * @param message Wiadomość do wysłania, do innego użytkownika
     */
    void sendMessage(String message) {
        try {
            printWriter.println(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Metoda klasy ServerThread zwracająca numer portu socketu serwera
     * @return Numer portu socketu serwera
     */
    public int getSocketLocalPort() {
        return serverSocket.getLocalPort();
    }

    /**
     * Metoda klasy ServerThread zamykająca socket serwera
     */
    public void socketClose() {
        try {
            serverSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
