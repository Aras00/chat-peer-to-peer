@echo off



cd src

javac -J-Dfile.encoding=UTF-8 PeertoPeer\*.java

jar cfe ..\chat.jar PeertoPeer.Peer PeertoPeer\*.class

del PeertoPeer\*.class

cd ..
